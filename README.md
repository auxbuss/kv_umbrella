# KvUmbrella

This project implements the project built during the
[Dependencies and umbrella projects](http://elixir-lang.org/getting-started/mix-otp/dependencies-and-umbrella-apps.html)
section of the official elixir documentation.


## Testiing

Without distributed tests, at project root run:

    $ mix test

For distributed tests, create a node via:

    $ cd apps/kv
    $ iex --sname foo -S mix

Then run the tests:

    $ elixir --sname bar -S mix test --include distributed

The nodes can be tested manually (local node):

    iex(foo@MBP)> KV.Router.route("hello", Kernel, :node, [])
    :foo@MBP
    iex(foo@MBP)> apply(Kernel, :node, [])
    :foo@MBP

The remote note can also be tested, provided it is loaded:

    iex(foo@MBP)> KV.Router.route("world", Kernel, :node, [])
    :bar@MBP
